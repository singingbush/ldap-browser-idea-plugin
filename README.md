# LDAP Browser plugin for Jetbrains products

This is an LDAP browser plugin for [IntelliJ IDEA](https://www.jetbrains.com/idea/) and other Jetbrains products.

Releases are available from the [Jetbrains plugin repository](https://plugins.jetbrains.com/plugin/8513).

### Building the plugin

To compile the plugin simply run `./gradlew buildPlugin` in the root of the project. 

Alternatively, to run the plugin in a sandboxed IDE use `./gradlew runIde`
